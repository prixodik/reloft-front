
jQuery(function($){

var $window = $(window);
var ww = $window.width(); 
var wh = $window.height();
var xxxs = 320;
var xxs = 550;
var xs = 724;
var sm = 959;
var md = 1101;
var headerPos = $(".header__bottom").offset().top;

var $elSticky = $(".js-product-sticky");
if($elSticky.length){
	var productRightPos = $elSticky.offset().top;
}

if( $('.js-category-grid').length ){
	/*$('.js-category-grid').masonry({
		// options
		itemSelector: '.js-grid-item',
		columnWidth: '.js-grid-item',
		percentPosition: false,
		gutter: 0,
		horizontalOrder: false,
		fitWidth: true
	});*/
}

var lazy = lazyStart();
/*var lazy = $('.lazy').Lazy({
	    // your configuration goes here
	    //placeholder: "data:image/gif;base64,R0lGODlhEALAPQAPzl5uLr9Nrl8e7...",
	    scrollDirection: 'vertical',
	    effect: 'fadeIn',
	    //visibleOnly: true,
	    chainable: false,
	    afterLoad: function(element) {
	            // called after an element was successfully handled
	            console.log('load element '+ element);
	        },
	    onError: function(element) {
	        console.log('error loading ' + element.data('src'));
	    }
	});*/
//var lazy = $('.lazy').data("plugin_lazy");


//console.log(lazy);

// Фиксация шапки
$(document).scroll(function(){
	var $elSticky = $(".js-product-sticky");
	
	if( $("html,body").scrollTop() >= headerPos ){
		$(".header").addClass("fixed");
	}else{
		$(".header").removeClass("fixed");
	}

	if( $("html,body").scrollTop() >= productRightPos ){
		//$elSticky.width( $elSticky.width() ).addClass("fixed");
		$elSticky.addClass("fixed");
	}else{
		$elSticky.removeClass("fixed");
	}
});

// Количество +-
$(".js-count-plus").click(function(){
	var count = $(this).parent().children('input').val();
	$(this).parent().children('input').val(++count);
	return false;
});

$(".js-count-minus").click(function(){
	var tempCount = $(this).parent().children('input').val();
	var count = (--tempCount <= 0)? 1 : --tempCount;

	$(this).parent().children('input').val(count);
	return false;
});

// Мобильное меню
$(".mobile-menu__item.parent > a").click(function(){
	$('.mobile-menu__item').removeClass('selected');
	$(this).parent().addClass('selected');
	return false;
});

$(".js-back-menu").click(function(){
	$(this).parents(".mobile-menu__item").removeClass('selected');
	return false;
});
// Мобильное меню End

$(".js-show-submenu").hover(function(){
	var showItem = $(this).attr("data-id");
	//showItem = showItem.substr(1,showItem.length);
	
	$(this).parent().siblings().removeClass("active");
	$(this).parent().addClass("active");

	$(this).parents(".main-menu__sub").nextAll().find('.main-menu__subitem[data-parent]').removeClass('show');
	$(this).parents(".main-menu__col").nextAll().find('.main-menu__banner').addClass("show");	

	$(this).parents(".main-menu__col").nextAll().find('.main-menu__banner, .main-menu__subitem[data-parent]').removeClass('show');

	if($(this).parents(".main-menu__col").next().next().find('.main-menu__subitem[data-parent="'+showItem+'"]').length == 0){ // У последней колонки нет subitem
		$(this).parents(".main-menu__col").next().next().find('.main-menu__banner').addClass('show');
	}

	$(this).parents(".main-menu__col").nextAll().find('.main-menu__subitem[data-parent="'+showItem+'"]').addClass("show");	
	
});

$(".main-menu__item.parent").hoverDelay({
        over: function(e) { 
        	if( $(this).hasClass("hover") == false){
				$(this).addClass("hover");
			}
        }, // mouseover handler
        out: function(e) { 
        	$(this).removeClass("hover");
        }, // mouseout handler
        delayOver: 150, // fire mouseover after 350 milliseconds
        delayOut: 350 // fire mouseout immediately
});

// Разворот текстового описания
$(".js-more-text").click(function(){
	if( $(this).hasClass('hide') == false ){
		$(this).addClass('hide');
		$(this).parent().parent().find(".js-wrap-text").addClass("show");
	}else{
		$(this).removeClass('hide');
		$(this).parent().parent().find(".js-wrap-text").removeClass("show");
	}
	return false;
});

$(".js-view-grid").click(function(){
	$(this).parents(".js-view").find(".view-block__item").removeClass("active");
	$(this).parent().addClass("active");
	$(".shop-page__list").removeClass("shop-page__list--view-list");
	$(".product-block").removeClass("product-block--list");
	return false;
});

$(".js-view-list").click(function(){
	$(this).parents(".js-view").find(".view-block__item").removeClass("active");
	$(this).parent().addClass("active");
	$(".shop-page__list").addClass("shop-page__list--view-list");
	$(".product-block").addClass("product-block--list");
	return false;
});

// Фильтр цвета
$(".js-color-hint-show").click(function(){
	$(this).parents(".color-block").toggleClass('active');
	offsetColor($(this));
	return false;
});

$(".js-checked-color").click(function(){
	if( $(this).children('input').prop('checked') == true){
		var img = $(this).find('img').attr("src");
		$('<img src="'+img+'" alt="">').appendTo(".color-block__current__list");
		$(".color-block__current__img").hide();
	}else{
		var img = $(this).find('img').attr("src");
		$(".color-block__current__list").find('img[src="'+img+'"]').remove();
		if( $(".color-block__current__list img:not(.color-block__current__img)").length <= 0){
			$(".color-block__current__img").show();
		}
	}

	offsetColor($(this));

});

$(".js-reset-colors").click(function(){
	$(this).parents(".color-block").find('.color-block__list .checkbox input').prop('checked',false);
	$(".color-block__current__list img:not(.color-block__current__img)").remove();
	$(".color-block__current__img").show();

	offsetColor($(this));

});

function offsetColor($i){
	//var $i = $(this);
	var $hint = $i.parents(".color-block").find(".color-block__hint");
	var offsetRight = ($hint.offset().left + $hint.width());
	
	if( $hint.offset().left <= 0){
		$hint.toggleClass('color-block__hint--left');
	}else if(offsetRight > ww){
		$hint.removeClass('color-block__hint--left');
	}

	setTimeout(function(){
		if( $hint.offset().left <= 0){
			$hint.toggleClass('color-block__hint--left');
		}else if(offsetRight > ww){
			$hint.removeClass('color-block__hint--left');
		}
	},50);
}
// Фильтр цвета end

$(document).on('click', '.js-filter-submit', function() {
	sendFilter();
	return false;
});

if(ww > xs){
	$(document).on('change', '.js-filter-block select, .js-filter-block input[type=radio]:not(.change-color):not(.change-view), .js-filter-block input[type=checkbox]:not(.change-color):not(.change-view)', function() { console.log('1');
	      sendFilter();
	});
}

$('.js-filter-reset').click(function(){
	$(this).parents('.js-filter-block').find('input[type=radio], input[type=checkbox]').prop('checked',false);
	$(".color-block__current__list img:not(.color-block__current__img)").remove();
	$(".color-block__current__img").show();

	$(this).parents('.js-filter-block').find('.js-range-slider').each(function(){
		var my_range = $(this).data("ionRangeSlider");
		my_range.reset();
	});

	$(this).parents('.js-filter-block').find('.nice-select').each(function(){
		var select = $(this).prev('select').children('option:selected');
		
		select.prop('selected',false);
		$(this).addClass("noselect").find('.option').removeClass("selected");
		var defaultText = $(this).find('.option:first-child').text();
		$(this).find('.current').text(defaultText);
	});
	return false;
});

$('.js-mob-filter-show').click(function(){
	$(".mobile-block").removeClass("active");
	$(".filter-block__wrapper").addClass('active');
	return false;
});

$('.js-mob-filter-hide').click(function(){
	$('body').removeClass('overflow');
	$(".filter-block__wrapper").removeClass('active');
	return false;
});

$(".js-filter-toggle").click(function(){
	$(this).parents(".js-filter-block").find(".filter-block__item[data-toggle]").toggleClass("hidden");
	if( $(this).children('span').text() == "Все параметры"){
		$(this).addClass("active").children('span').text("Свернуть");
	}else{
		$(this).removeClass("active").children('span').text("Все параметры");
	}
	return false;
});

if( $(".js-range-slider").length ){ 
	$(".js-range-slider").each(function(){
		var fr = $(this).parents(".range-block").find(".range-block__from");
		var frInput = $(this).parents(".range-block").find(".range-block__from-input");
		var to = $(this).parents(".range-block").find(".range-block__to");
		var toInput = $(this).parents(".range-block").find(".range-block__to-input");
		var $inp = $(this);
		
		$(this).ionRangeSlider({
			skin: "round",
			hide_from_to: true,
			onStart: function(data){
				$(fr).text( data.from );
				$(frInput).val( data.from );
				$(to).text( data.to );
				$(toInput).val( data.to );
			},
			onChange: function (data) {
	            // Called every time handle position is changed
	    		$(fr).text( data.from );
	    		$(frInput).val( data.from );
	    		$(to).text( data.to );
	    		$(toInput).val( data.to );
	            //console.log(data);
	        },
	        onFinish: function(data){
	        	sendFilter(); //Отправка фильтра
	        }
		});

		frInput.on("keyup", function () {
	        var my_range = $inp.data("ionRangeSlider");
		    my_range.update({
		        from: $(this).val()
		    });
	    });

	    toInput.on("keyup", function () {
	        var my_range = $inp.data("ionRangeSlider");
		    my_range.update({
		        to: $(this).val()
		    });
	    });
	});
}

// Добавление в карзину
$(document).on('click', '.js-add-cart', function() {
	addCart();
	return false;
});

if( $("select").length && ww > xs){
	$('select').niceSelect();
}

if( $(".js-input-phone").length ){
	$("input.js-input-phone").mask("+7(999) 999-99-99");
}

if( $(".fancybox").length ){
	$(".fancybox").fancybox({
		// Options will go here
		iframe : {
			preload : false
		}
	});
}

$(".js-scroll-to").click(function(){
	var href = $(this).attr("href");
	$('html, body').animate({ scrollTop: $(href).offset().top }, 400);
	return false;
});

$(".js-popup-to").click(function(){
	var href = $(this).attr("href");

	$(".mobile-block").removeClass("active");

	$(".popup-wrap").removeClass("active");
	$(href).addClass("active");
	
	$("body").addClass("popup-show");
	
	return false;
});

$(".js-popup-close").click(function(){
	$(".popup-wrap").removeClass("active");
	$("body").removeClass("popup-show");
	return false;
});

// Переключение табов
$(".js-tab-nav a").click(function(){
	var href = $(this).attr("href");
	var nav = $(this).parent();
	var wrap = $(this).parents(".js-tabs-wrapper");

	wrap.find(".js-tab-box").removeClass("active");
	wrap.find(".js-tab-nav").removeClass("active");

	nav.addClass("active");

	$(href).addClass("active");
	
	return false;
});



// Закрыть мобильное меню
$(".js-close-menu").click(function(){
	$("body").removeClass("popup-show");
	$(".mobile-block").removeClass("active");
	$(".mobile-menu__item").removeClass("selected");
	return false;
});

// Добавление товара в избранные
$(document).on('click', '.js-add-favorite', function(){
	var $e = $(this);
	var id = $(this).data('id'); // id товара
	var type = ( $e.hasClass('active') == false)? 'add' : 'remove'; // Тип запроса Добавление, Удаление
	var url = ( $e.hasClass('active') == false)? '/ajax/addFavorite.json' : '/ajax/removeFavorite.json'; // Результат запроса Добавление, Удаление (Удалить)
	
	$.getJSON(url, {'id': id, 'type': type}, function(data) {
        if(data.result == 'add'){
        	$e.addClass('active');
        	$('.js-favorite-value').text(data.counter);
        }else{
        	$e.removeClass('active');
        	$('.js-favorite-value').text(data.counter);
        }
    });
	return false;
});

// Смена вида отображения товаров
$('.js-view-type label').click(function(){ 
	var forId = $(this).attr('for');
	var view = $('input#'+forId).val();

	$('.main-section__list').removeClass('main-section__list--view-1').removeClass('main-section__list--view-2').removeClass('main-section__list--view-4').addClass('main-section__list--'+view);

	$('.product-block').removeClass('product-block--view-1').removeClass('product-block--view-2').removeClass('product-block--view-4').addClass('product-block--'+view);
//	return false;
});

if( $(".js-index-slider-img").length || $(".js-index-slider-info").length){

	$(".js-index-slider-img").slick({
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			lazyLoad: 'progressive',
			arrows: false,
			asNavFor: $('.js-index-slider-info')
	});

	$(".js-index-slider-info").slick({
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			lazyLoad: 'progressive',
			arrows: true,
			fade: true,
			nextArrow: '<button type="button" class="slider-section__next slick-next slick-arrow slick-arrow--circle slick-arrow--white"><svg class="icon"><use xlink:href="/images/sprite.svg#arrow-right-2"></svg></button>',
			prevArrow: '<button type="button" class="slider-section__prev slick-prev slick-arrow slick-arrow--circle slick-arrow--white"><svg class="icon"><use xlink:href="/images/sprite.svg#arrow-left-2"></svg></button>',
			responsive: [{
		    	breakpoint: xs,
		      	settings: {
		      		arrows: false,
		        	dots: true,
		        	appendDots: $('.js-slider-section-dots')
		      	}
		    }]
	});

}

if( $(".js-product-gallery-slick").length){
	$(".js-product-gallery-slick").slick({
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			lazyLoad: 'progressive',
			nextArrow: '<button type="button" class="product-page__img__next slick-next"><svg class="icon"><use xlink:href="/images/sprite.svg#arrow-right"></svg></button>',
			prevArrow: '<button type="button" class="product-page__img__prev slick-prev"><svg class="icon"><use xlink:href="/images/sprite.svg#arrow-left"></svg></button>',
			responsive: [{
		    	breakpoint: xxs,
		      	settings: {
		        	dots: true
		      	}
		    }]
	});

	$('.js-product-gallery-slick').on('beforeChange', function(event, slick, currentSlide, nextSlide){
		$(".js-product-gallery-nav").removeClass('active');
		$(".js-product-gallery-nav:nth-of-type("+(++nextSlide)+")").addClass('active');
	});

	$(".js-product-gallery-nav").click(function(){
		var index = $(this).index();
		$(".js-product-gallery-nav").removeClass('active');
		$(this).addClass('active');
		$(".js-product-gallery-slick").slick("slickGoTo", index);
		return false;
	});
}

if( $(".js-other-slick").length){
		$(".js-other-slick").slick({
			infinite: true,
			slidesToShow: 4,
			slidesToScroll: 4,
			lazyLoad: 'progressive',
			nextArrow: '<button type="button" class="product-page__other__next"><svg class="icon"><use xlink:href="/images/sprite.svg#arrow-right"></svg></button>',
			prevArrow: '<button type="button" class="product-page__other__prev"><svg class="icon"><use xlink:href="/images/sprite.svg#arrow-left"></svg></button>',
			responsive: [{
		    	breakpoint: xxs,
		      	settings: {
		      		infinite: false,
		      		arrows: false,
		      		variableWidth: true,
		        	slidesToShow: 1,
		        	slidesToScroll: 1,
		      	}
		    },{
		    	breakpoint: md,
		      	settings: {
		        	slidesToShow: 3,
		        	slidesToScroll: 3,
		      	}
		    },{
		    	breakpoint: sm,
		      	settings: {
		        	slidesToShow: 2,
		        	slidesToScroll: 2,
		      	}
		    }]
		});
}

if( $(".js-new-product-slick").length){
		$(".js-new-product-slick").slick({
			infinite: true,
			slidesToShow: 4,
			slidesToScroll: 4,
			lazyLoad: 'progressive',
			nextArrow: '<button type="button" class="new-product-section__next slick-arrow slick-arrow--circle slick-arrow--border"><svg class="icon"><use xlink:href="/images/sprite.svg#arrow-right-2"></svg></button>',
			prevArrow: '<button type="button" class="new-product-section__prev slick-arrow slick-arrow--circle slick-arrow--border"><svg class="icon"><use xlink:href="/images/sprite.svg#arrow-left-2"></svg></button>',
			responsive: [{
		    	breakpoint: xxs,
		      	settings: {
		      		infinite: false,
		      		arrows: false,
		      		variableWidth: true,
		        	slidesToShow: 1,
		        	slidesToScroll: 1,
		      	}
		    },{
		    	breakpoint: md,
		      	settings: {
		        	slidesToShow: 3,
		        	slidesToScroll: 3,
		      	}
		    },{
		    	breakpoint: sm,
		      	settings: {
		        	slidesToShow: 2,
		        	slidesToScroll: 2,
		      	}
		    }]
		});
}

if( $(".js-artist-slider").length){
		$(".js-artist-slider").slick({
			infinite: true,
			slidesToShow: 5,
			slidesToScroll: 5,
			lazyLoad: 'progressive',
			nextArrow: '<button type="button" class="new-artist-section__next slick-arrow slick-arrow--circle slick-arrow--border"><svg class="icon"><use xlink:href="/images/sprite.svg#arrow-right-2"></svg></button>',
			prevArrow: '<button type="button" class="new-artist-section__prev slick-arrow slick-arrow--circle slick-arrow--border"><svg class="icon"><use xlink:href="/images/sprite.svg#arrow-left-2"></svg></button>',
			responsive: [{
		    	breakpoint: xxs,
		      	settings: {
		      		infinite: false,
		      		arrows: false,
		      		variableWidth: true,
		        	slidesToShow: 1,
		        	slidesToScroll: 1,
		      	}
		    },{
		    	breakpoint: md,
		      	settings: {
		        	slidesToShow: 4,
		        	slidesToScroll: 4,
		      	}
		    },{
		    	breakpoint: sm,
		      	settings: {
		        	slidesToShow: 3,
		        	slidesToScroll: 3,
		      	}
		    }]
		});
}

if( $(".js-brands-slider").length){
		$(".js-brands-slider").slick({
			infinite: true,
			slidesToShow: 5,
			slidesToScroll: 5,
			lazyLoad: 'progressive',
			nextArrow: '<button type="button" class="brands-section__next slick-arrow slick-arrow--circle slick-arrow--border"><svg class="icon"><use xlink:href="/images/sprite.svg#arrow-right-2"></svg></button>',
			prevArrow: '<button type="button" class="brands-section__prev slick-arrow slick-arrow--circle slick-arrow--border"><svg class="icon"><use xlink:href="/images/sprite.svg#arrow-left-2"></svg></button>',
			responsive: [{
		    	breakpoint: xxs,
		      	settings: {
		        	slidesToShow: 1,
		        	slidesToScroll: 1,
		      	}
		    },{
		    	breakpoint: md,
		      	settings: {
		        	slidesToShow: 4,
		        	slidesToScroll: 4,
		      	}
		    },{
		    	breakpoint: sm,
		      	settings: {
		        	slidesToShow: 3,
		        	slidesToScroll: 3,
		      	}
		    }]
		});
}

if(ww <= sm){ // Модернизация под мобильные sm
	smModernization();
}

if(ww <= xs){ // Модернизация под мобильные xs
	xsModernization();
}



$window.load(function(){
	// Прокрутка в выпадающем дропдауне
	if( $(".nice-select .list").length ){
		/*$('.nice-select .list').jScrollPane({
			resizeSensor: true
		});*/
	}

});

$window.resize(function(){
	if($window.width() != ww){
		ww = $window.width();
		wh = $window.height();
	
		if(ww <= sm){
			smModernization();
		}
		if(ww <= xs){
			xsModernization();
		}
	}	
});

function smModernization(){
	if( $(".js-sm-slick").length){
		$(".js-sm-slick").slick({
			infinite: true,
			slidesToShow: 2,
			slidesToScroll: 2,
			lazyLoad: 'progressive',
			nextArrow: '<button type="button" class="slick-next"><svg class="icon"><use xlink:href="/images/sprite.svg#arrow-right"></svg></button>',
			prevArrow: '<button type="button" class="slick-prev"><svg class="icon"><use xlink:href="/images/sprite.svg#arrow-left"></svg></button>',
			responsive: [{
		    	breakpoint: xs,
		      	settings: {
		        	slidesToShow: 1,
		        	slidesToScroll: 1,
		      	}
		    }]
		});
	}

	// Показать мобильное меню
	$(".js-show-menu").click(function(){
		$("body").toggleClass("overflow");
		$(".mobile-block").toggleClass("active");
		return false;
	});

}

function xsModernization(){
	$(".footer__contacts").appendTo(".footer__body .footer__container");
	$(".js-filter-item-order-xs-2").prependTo( $('.filter-block__top .filter-block__row') );
	$(".js-filter-item-order-xs-1").prependTo( $('.filter-block__top .filter-block__row') );
	$('.js-color-hint-show').insertAfter('.color-block__title');
	$(".js-product-gallery-slick").insertAfter('.product-page__head');

	if( $(".js-xs-slick").length){
		$(".js-xs-slick").slick({
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			nextArrow: '<button type="button" class="slick-next"><svg class="icon"><use xlink:href="/images/sprite.svg#arrow-right"></svg></button>',
			prevArrow: '<button type="button" class="slick-prev"><svg class="icon"><use xlink:href="/images/sprite.svg#arrow-left"></svg></button>',
		});
	}
}

function addCart(){
	
	var $form = $('form.js-product-form');
	var url = $form.attr('action');
	var datas = $form.serialize();
	
	$.getJSON(url, datas, function(data) {
	
        if(data.result == 'add'){
        	//$e.addClass('active');
        	$('.js-cart-value').text(data.counter);
        }

    }).error(function(){
    	console.log("error");
    });
	return false;
}

function sendFilter(){
	
	var $form = $('.js-filter-block form');
	var url = $form.attr('action');
	var datas = $form.serialize();
	var view = $form.find('input.change-view:checked').val();

	$.getJSON(url, datas, function(data) {
	
        if(data.result){
        	var result = data.result;
        	var template = '';

        	for (var i = 0; i < result.length; i++) {

        		var label = (result[i].label != undefined && result[i].label != "") ? '<div class="product-block__label">'+result[i].label+'</div>' : '';

        		var favorite = (result[i].favorite == true)? 'active' : '';

        		var oldPrice = (result[i].oldPrice != undefined && result[i].oldPrice != "")? '<span class="product-block__old-price">'+result[i].oldPrice+' руб.</span>' : ''

        		var params = '';
        		for (var j = 0; j < result[i].params.length; j++) {
        			var paramLink = (result[i].params[j].link != "" && result[i].params[j].link != undefined)? '<a href="'+result[i].params[j].link+'">'+result[i].params[j].value+'</a>' : result[i].params[j].value;
        			params += '<div class="product-block__param"><div class="product-block__param__label">'+result[i].params[j].label+':</div><div class="product-block__param__value">'+paramLink+'</div></div>';
        		}
        		

        		template += '<div class="main-section__list__item"><div class="product-block product-block--'+view+'"><div class="product-block__img">'+label+'<a class="product-block__favorite js-add-favorite '+ favorite +'" href="#" data-id="'+result[i].id+'"><svg class="icon"><use xlink:href="images/sprite.svg#icon-favorite"></use></svg></a><a href="'+result[i].link+'"><img class="lazy" src="'+result[i].thumb+'" data-src="'+result[i].img+'" data-srcset="'+result[i].img+' 1x, '+result[i].img2x+' 2x" alt="'+result[i].title+'"></a></div><div class="product-block__body"><div class="product-block__title">'+result[i].title+'</div>'+params+'<div class="product-block__price"><span class="product-block__price__label">Цена:</span> '+ oldPrice +' '+result[i].price+' руб.</div></div></div></div>';
        	}

        	$('.js-products-list-ajax').empty().html(template);
        }

    }).error(function(){
    	console.log("error");
    });
	return false;
}

function lazyStart(){
	var lazy = $('.lazy').Lazy({
	    scrollDirection: 'vertical',
	    effect: 'fadeIn',
	    visibleOnly: true,
	    afterLoad: function(element) {
	            // called after an element was successfully handled
	            //console.log('load element '+ element);
	        },
	    onError: function(element) {
	        //console.log('error loading ' + element.data('src'));
	    }
	}).removeClass("lazy");

	$(document).ajaxStop(function(){
	    $('.lazy').Lazy({
	        scrollDirection: 'vertical',
		    effect: 'fadeIn',
		    visibleOnly: true,
		    afterLoad: function(element) {
		            // called after an element was successfully handled
		            // console.log('load element '+ element);
		        },
		    onError: function(element) {
		        console.log('error loading ' + element.data('src'));
		    }
	    }).removeClass("lazy");
	});
	return lazy;
}

});	



